from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='netbox-peachi-saml2',
    version='1.1',
    description='Netbox plugin for Peachi SAML2 auth backend',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Josh Harris',
    license='Apache 2.0',
    install_requires=[],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
)
