from extras.plugins import PluginConfig


class PeachiSAML2Plugin(PluginConfig):
    name = 'netbox_peachi_saml2'
    verbose_name = 'Netbox Peachi SSO SAML2 plugin'
    description = 'Netbox plugin for Peachi SAML2 auth backend'
    version = '1.1'
    author = 'Josh Harris'

    required_settings = []
    default_settings = {}


config = PeachiSAML2Plugin
