from django.contrib.auth.models import User
from django.core.handlers.wsgi import WSGIRequest
from saml2.response import AuthnResponse

# Subclass from the Netbox provided RemoteUserBackend so that we get the
# benefits of the REMOTE_AUTH_DEFAULT_GROUPS and
# REMOTE_AUTH_DEFAULT_PERMISSIONS

from netbox.authentication import RemoteUserBackend


class SAML2AttrUserBackend(RemoteUserBackend):
    def configure_user(self, request: WSGIRequest, user: User) -> User:
        """
        This method is only called when a new User is created.  This method
        will use the SAML2 user identity to configure addition properies about
        the user.  This will include:

           * first_name
           * last_name
           * email
        """

        saml2_auth_resp: AuthnResponse = request.META['SAML2_AUTH_RESPONSE']
        user_ident = saml2_auth_resp.get_identity()

        try:
            user.first_name = user_ident['first_name'][0]
            user.last_name = user_ident['last_name'][0]
            user.email = user_ident['email'][0]
            user.save()

        except KeyError as exc:
            missing_attr = exc.args[0]
            be_name = self.__class__.__name__
            raise PermissionError(f"SAML2 backend {be_name} missing attribute: {missing_attr}")

        # call Netbox superclass for further processing of REMOTE_AUTH_xxx variables.
        return super().configure_user(request, user)
