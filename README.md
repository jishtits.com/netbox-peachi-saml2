# Netbox Plugin for SSO using SAML2 (Customised for Peachi SAML2 Attributes)

This is a remote auth backend for NetBox designed to be used in conjunction with the original [Netbox Plugin for SSO using SAML2](https://github.com/jeremyschulman/netbox-plugin-auth-saml2) by Jeremy Schulman.

We are mostly using the `SAML2AttrUserBackend` from their repo, but modified to respect our SAML attributes.

## Required Attributes

This remote auth backend requires the following SAML2 attributes to be present:
```
username
first_name
last_name
email
```

## System Requirements

You will need to install the [django3-auth-saml2](https://github.com/jeremyschulman/django3-auth-saml2) pip dependency
in your Netbox environment, as well as the `xmlsec1` system package.

## License

This code (and the original by Jeremy Schulman) are both licensed under Apache 2.0. You may obtain a copy of the license at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

